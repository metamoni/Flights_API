import React from 'react';
import FlightDetails from './FlightDetails';

const FlightList = (props) => {

	const flights = props.flights.map((flight) => {

  if(flight.ArrDep === "A") {
		return (
			<article className="container" key={flight._id}>
				<h2>FROM {flight.PortOfCallA}</h2>
				<FlightDetails flight={flight}/>
			</article>
	  )
	} else {
		  return (
			    <article className="container" key={flight._id}>
				    <h2>TO {flight.PortOfCallA}</h2>
						<FlightDetails flight={flight}/>
			    </article>
	    )
	  }
	})

	return (
	  <main>
	    {flights}
	  </main>

	)
}
 export default FlightList;
