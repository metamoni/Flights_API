import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';

class Navbar extends Component  {
  render() {
    return (
          <header>
            <nav className="white">
              <ul className="right">
                <li>
                  <NavLink className="navLink" to="/">Home</NavLink>
                </li>
                <li>
                  <NavLink className="navLink" to="/flights">All flights</NavLink>
                </li>
                <li>
                  <NavLink className="navLink" to="/arrivals">Arrivals</NavLink>
                </li>
                <li>
                  <NavLink className="navLink" to="/departures">Departures</NavLink>
                </li>
              </ul>
            </nav>
          </header>
    )
  }
}

export default withRouter(Navbar);
