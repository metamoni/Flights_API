import React, { Fragment } from 'react';

const FlightDetails = ({flight}) => {

    return(
      <Fragment>
        <img className="airline-logo" alt="Airline logo" src={flight.Image} />
        <p>Flight number: {flight.FlightNo}</p>
        <p>Airline: {flight.Airline}</p>
        <p>Date: {flight.Date}</p>
        <p>Time: {flight.Time}</p>
        <p>Status: {flight.Status}</p>
        <p>Arrival hall used: {flight.ArrHall}</p>
        <p>Other info: {flight.OtherInfo}</p>
      </Fragment>
    )
}

export default FlightDetails;