import React from 'react';
import { Link } from 'react-router-dom';

const Home = (props) => {
    return(
      <section className="container">
        <h1>Welcome to Edinburgh Airport</h1>
        <p className="flow-text">Explore Scotland, famous for its historic castles, majestic highland cattle and beautiful lochs</p>
        <Link to="/flights">
          <button className="btn-large purple darken-3">View all flights</button>
        </Link>
      </section>
    )
}

export default Home;