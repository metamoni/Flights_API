import React, { Component } from 'react';
import FlightList from '../components/FlightList.js';
import Request from '../helpers/request.js';

class Arrivals extends Component {
    state = {
        arrivals: []
    }

    componentDidMount() {
        let request = new Request();
        request.get('/api/arrivals').then((data) => {
            this.setState({ arrivals: data })
        })
    }

    render() {
        return(
            <FlightList flights={this.state.arrivals} />
        )
    }
}

export default Arrivals;