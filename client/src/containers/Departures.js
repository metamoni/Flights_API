import React, { Component } from 'react';
import FlightList from '../components/FlightList.js';

import Request from '../helpers/request.js';

class Departures extends Component {
    state = {
        departures: []
    }

    componentDidMount() {
        let request = new Request();
        request.get('/api/departures').then((data) => {
            this.setState({ departures: data })
        })
    }

    render() {
        return(
            <FlightList flights={this.state.departures} />
        )
    }
}

export default Departures;