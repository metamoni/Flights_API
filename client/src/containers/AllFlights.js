import React, { Component } from 'react';
import FlightList from '../components/FlightList.js';

import Request from '../helpers/request.js';

class AllFlights extends Component {
    state = {
        flights: []
    }

    componentDidMount() {
        let request = new Request();
        request.get('/api/flights').then((data) => {
            this.setState({ flights: data })
        })
    }

    render() {
        return(
          <FlightList flights={this.state.flights} />
        )
    }
}

export default AllFlights;