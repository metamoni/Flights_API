import React, { Component, Fragment } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Navbar from './components/Navbar.js';
import Home from './components/Home.js';
import AllFlights from './containers/AllFlights.js';
import Arrivals from './containers/Arrivals.js';
import Departures from './containers/Departures.js';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <Navbar />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/flights" component={AllFlights} />
            <Route path="/arrivals" component={Arrivals} />
            <Route path="/departures" component={Departures} />
          </Switch>
        </Fragment>
      </BrowserRouter>
    )
  }
}

export default App;
