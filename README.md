# Flights_API

This project was built using **javaScript**, **Express**, and **MongoDB**. I’ve chosen to import the provided json file into a database called *airport*.

# MVP
As per the assignment requirements, I've set up the following endpoints:
* /api/flights
* /api/arrivals
* /api/departures
* /api/flight/:number (replace ':number' with any value from the "FlightNo" field)

Errors are caught and handled, but without redirecting the user to a custom error page.


# Additional features
For this assignment I've also set up a basic SPA using React. It has a menu with links pointing to all flights, arrivals, and departures, but linking to a single flight hasn't been set up yet. The '/api/flight/:number' URL can only be accessed from the back-end.


## Setup
In your Terminal go to the project folder and type the following commands:
* `npm i` to install back-end dependencies
* type `mongod`, leaving this tab open
* in a new tab, type `mongo` to start the mongo shell and create a database by typing `use airport`; you can now exit the mongo shell by typing `exit`
* import the json file by typing `mongoimport --db airport --collection flights --file ~/Downloads/API/flights.json --jsonArray`; make sure to replace the file path with the correct file path on your computer
* switch to the client folder with `cd client` and run `npm i` to install front-end dependencies
* to start both the front and back ends simultaneously, type `npm run dev` from the main project folder
* alternatively, you can type `npm run server` from the main project folder and visit [localhost:5000/](http://localhost:5000/api/flights) to access the endpoints; then navigate to the client folder and type `npm start` to fire up the front-end, and go to [localhost:3000/](http://localhost:3000) in your browser