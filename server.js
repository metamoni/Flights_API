const express = require('express');
const app = express();
const parser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const createRouter = require('./helpers/create_router.js');
const path = require('path');

const publicPath = path.join(__dirname, '../client/public');
app.use(express.static(publicPath));

app.use(parser.json());

// connect to 'airport' database
// and then to the 'flights' collection
MongoClient.connect('mongodb://localhost:27017', { useNewUrlParser: true })
  .then((client) => {
  const db = client.db('airport');
  const flightsCollection = db.collection('flights');
  const flightsRouter = createRouter(flightsCollection);
  // initialize routes
  app.use('/api', flightsRouter);
  })
  .catch(console.err);

// listen on port 5000 instead of default 3000
const port = process.env.PORT || 5000;

app.listen(port, function() {
  console.log(`Server started on port ${this.address().port}`);
});